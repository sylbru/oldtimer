const gulp = require("gulp")
const elm = require("gulp-elm")
const uglify = require("gulp-uglify")

function watch(cb) {
    gulp.watch(["src/*.elm"], function compile (cb) {
        console.log("Recompiling…")
        build(() => { console.log("✔ Done!") })
        cb()
    })
    cb()
}

function build(cb) {
    gulp.src(["src/Main.elm"])
        .pipe(elm())
        .on("error", error => console.log(error.toString()))
        .pipe(uglify({ compress: false, mangle: false }))
        .pipe(gulp.dest("dist/js"))
    cb()
}

exports.default = watch
exports.watch = watch
exports.build = build