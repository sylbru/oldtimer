module Times exposing (absoluteDay, addMinutes, earlierBy, laterBy, monthToInt, roundTime, viewDay, viewDayFriendly, viewDuration, viewTime, viewTimeWithSeconds, weekdayToString)

import Time exposing (Month, Weekday(..))


earlierBy : Int -> Time.Posix -> Time.Posix
earlierBy granularity time =
    addMinutes (-granularity // 60) time


laterBy : Int -> Time.Posix -> Time.Posix
laterBy granularity time =
    addMinutes (granularity // 60) time


roundTime : Int -> Time.Posix -> Time.Posix
roundTime granularitySeconds time =
    let
        millis =
            Time.posixToMillis time

        granularity =
            granularitySeconds * 1000

        remainder =
            millis |> remainderBy granularity

        complement =
            granularity - remainder
    in
    (if remainder <= complement then
        -- we are closer to the lower rounded value
        -- (or exactly halfway between lower and upper)
        millis - remainder

     else
        -- we are strictly closer to the upper rounded value
        millis + complement
    )
        |> Time.millisToPosix


absoluteDay : Time.Zone -> Time.Posix -> Int
absoluteDay zone time =
    let
        dayMillis =
            24 * 60 * 60 * 1000

        timeAtMidnight zone_ time_ =
            Time.posixToMillis time_
                - (Time.toHour zone_ time_ * 3600 * 1000)
                - (Time.toMinute zone_ time_ * 60 * 1000)
                - (Time.toSecond zone_ time_ * 1000)
    in
    timeAtMidnight zone time // dayMillis


addMinutes : Int -> Time.Posix -> Time.Posix
addMinutes minutes time =
    time
        |> Time.posixToMillis
        |> (+) (minutes * 60 * 1000)
        |> Time.millisToPosix



---- View functions


viewDuration : Time.Posix -> Time.Posix -> Bool -> String
viewDuration timeStart timeEnd showSeconds =
    let
        millisStart =
            Time.posixToMillis timeStart

        millisEnd =
            Time.posixToMillis timeEnd

        totalSeconds =
            (millisEnd - millisStart) // 1000

        totalMinutes =
            totalSeconds // 60

        hours =
            totalMinutes // 60

        displayedSeconds =
            remainderBy 60 totalSeconds

        displayedMinutes =
            remainderBy 60 totalMinutes

        hoursPart =
            if hours > 0 then
                String.concat [ String.fromInt hours, "\u{00A0}h" ]

            else
                ""

        minutesPart =
            if displayedMinutes > 0 then
                String.concat [ String.fromInt displayedMinutes, "\u{00A0}min" ]

            else
                ""

        secondsPart =
            if displayedSeconds > 0 && showSeconds then
                String.concat [ String.fromInt displayedSeconds, "\u{00A0}s" ]

            else
                ""
    in
    if millisEnd < millisStart then
        "−\u{00A0}" ++ viewDuration timeEnd timeStart showSeconds

    else
        [ hoursPart, minutesPart, secondsPart ]
            |> List.filter (not << String.isEmpty)
            |> String.join " "


viewTime : Time.Zone -> Time.Posix -> String
viewTime zone time =
    (Time.toHour zone time |> String.fromInt)
        ++ ":"
        ++ (Time.toMinute zone time |> String.fromInt |> String.pad 2 '0')


viewTimeWithSeconds : Time.Zone -> Time.Posix -> String
viewTimeWithSeconds zone time =
    viewTime zone time ++ ":" ++ (Time.toSecond zone time |> String.fromInt |> String.pad 2 '0')


viewDay : Time.Zone -> Time.Posix -> String
viewDay zone time =
    String.join "/"
        [ Time.toDay zone time |> String.fromInt
        , Time.toMonth zone time |> monthToInt |> String.fromInt |> String.pad 2 '0'
        , Time.toYear zone time |> String.fromInt
        ]


viewDayFriendly : Time.Zone -> Time.Posix -> Time.Posix -> String
viewDayFriendly zone time now =
    let
        today =
            absoluteDay zone now
    in
    if absoluteDay zone time == today then
        "Aujourd’hui"

    else if absoluteDay zone time == today - 1 then
        "Hier"

    else
        weekdayToString (Time.toWeekday zone time)
            ++ " "
            ++ (Time.toDay zone time |> String.fromInt)
            ++ "/"
            ++ (Time.toMonth zone time |> monthToInt |> String.fromInt |> String.pad 2 '0')



-- Conversions


weekdayToString : Weekday -> String
weekdayToString weekDay =
    case weekDay of
        Mon ->
            "Lun."

        Tue ->
            "Mar."

        Wed ->
            "Mer."

        Thu ->
            "Jeu."

        Fri ->
            "Ven."

        Sat ->
            "Sam."

        Sun ->
            "Dim."


monthToInt : Time.Month -> Int
monthToInt month =
    case month of
        Time.Jan ->
            1

        Time.Feb ->
            2

        Time.Mar ->
            3

        Time.Apr ->
            4

        Time.May ->
            5

        Time.Jun ->
            6

        Time.Jul ->
            7

        Time.Aug ->
            8

        Time.Sep ->
            9

        Time.Oct ->
            10

        Time.Nov ->
            11

        Time.Dec ->
            12
