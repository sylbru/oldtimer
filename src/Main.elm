port module Main exposing (..)

import Browser exposing (Document)
import Debug exposing (..)
import Element as El exposing (Color, Element, rgb, rgb255)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as Dec
import Json.Encode as Enc
import Task
import Time
import Times exposing (absoluteDay, addMinutes, earlierBy, laterBy, monthToInt, roundTime, viewDay, viewDayFriendly, viewDuration, viewTime, viewTimeWithSeconds, weekdayToString)


type alias Model =
    { now : Time.Posix
    , timeZone : Time.Zone
    , timerRunningSince : Maybe Time.Posix
    , description : String
    , sessions : List Session
    , options : Options
    , editing : Maybe Session
    , uid : Int
    }


type alias StorableModel =
    { timerRunningSince : Maybe Int
    , description : String
    , sessions : List StorableSession
    , options : Options
    , uid : Int
    }


type alias Options =
    { granularity : Int -- in seconds
    }


type alias Session =
    { start : Time.Posix
    , end : Time.Posix
    , description : String
    , id : Int
    }


type alias StorableSession =
    { start : Int
    , end : Int
    , description : String
    , id : Int
    }


type Msg
    = SetTimeZone Time.Zone
    | SetCurrentTime Time.Posix
    | Start
    | StartAt Time.Posix
    | Stop
    | StopAt Time.Posix
    | UpdateCurrentDescription String
    | ExtendCurrentSession
    | ShortenCurrentSession
    | DeleteSession Int
    | EditSession Session
    | SessionEdition SessionEditionMsg


type SessionEditionMsg
    = UpdateDescription String
    | EarlierStartTime
    | LaterStartTime
    | EarlierEndTime
    | LaterEndTime
    | SaveChanges
    | DiscardChanges


init : Enc.Value -> ( Model, Cmd Msg )
init flags =
    let
        model =
            case Dec.decodeValue savedDataDecoder flags of
                Ok savedState ->
                    modelFromStorage savedState defaultModel

                Err error ->
                    defaultModel
    in
    ( model
    , Cmd.batch
        [ Task.perform SetTimeZone Time.here
        , Task.perform SetCurrentTime Time.now
        , setFaviconTimerRunning (model.timerRunningSince /= Nothing)
        ]
    )


defaultModel : Model
defaultModel =
    let
        epoch =
            Time.millisToPosix 0
    in
    { now = epoch
    , timeZone = Time.utc
    , timerRunningSince = Nothing
    , description = ""
    , sessions = []
    , options = Options (15 * 60)

    --, editing = Just (Session epoch (Time.millisToPosix (45 * 60 * 1000)) "" 1)
    , editing = Nothing
    , uid = 1
    }


updateAndSave : Msg -> Model -> ( Model, Cmd Msg )
updateAndSave msg model =
    let
        ( newModel, commands ) =
            update msg model
    in
    ( newModel
    , if messageTriggeringSave msg then
        Cmd.batch
            [ saveToLocalStorage (storableModel newModel)
            , commands
            ]

      else
        commands
    )


messageTriggeringSave : Msg -> Bool
messageTriggeringSave msg =
    case msg of
        Start ->
            False

        Stop ->
            False

        SetTimeZone _ ->
            False

        SetCurrentTime _ ->
            False

        _ ->
            True


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        granularity =
            model.options.granularity
    in
    case msg of
        UpdateCurrentDescription newDescription ->
            ( { model | description = newDescription }, Cmd.none )

        Start ->
            ( model
            , Cmd.batch
                [ Task.perform StartAt Time.now
                , setFaviconTimerRunning True
                ]
            )

        Stop ->
            ( model
            , Cmd.batch
                [ Task.perform StopAt Time.now
                , setFaviconTimerRunning False
                ]
            )

        StartAt startTime ->
            ( { model
                | timerRunningSince = Just (roundTime granularity startTime)
              }
            , Cmd.none
            )

        StopAt stopTime ->
            let
                newSession : Maybe Session
                newSession =
                    case model.timerRunningSince of
                        Just startTime ->
                            let
                                currentSessionDuration =
                                    (Time.posixToMillis (roundTime granularity stopTime)
                                        - Time.posixToMillis startTime
                                    )
                                        // 1000
                            in
                            if currentSessionDuration >= granularity then
                                Just
                                    (Session
                                        startTime
                                        (roundTime granularity stopTime)
                                        model.description
                                        model.uid
                                    )

                            else
                                Nothing

                        Nothing ->
                            Nothing
            in
            ( case newSession of
                Just session ->
                    { model
                        | timerRunningSince = Nothing
                        , sessions = session :: model.sessions
                        , uid = model.uid + 1
                    }

                Nothing ->
                    { model | timerRunningSince = Nothing }
            , Cmd.none
            )

        SetTimeZone timeZone ->
            ( { model | timeZone = timeZone }
            , Cmd.none
            )

        SetCurrentTime time ->
            ( { model | now = time }
            , Cmd.none
            )

        ExtendCurrentSession ->
            ( { model
                | timerRunningSince =
                    case model.timerRunningSince of
                        Nothing ->
                            Nothing

                        Just time ->
                            Just (earlierBy model.options.granularity time)
              }
            , Cmd.none
            )

        ShortenCurrentSession ->
            ( { model
                | timerRunningSince =
                    case model.timerRunningSince of
                        Nothing ->
                            Nothing

                        Just time ->
                            Just (laterBy model.options.granularity time)
              }
            , Cmd.none
            )

        DeleteSession id ->
            ( { model | sessions = List.filter (\session -> session.id /= id) model.sessions }
            , Cmd.none
            )

        EditSession session ->
            ( { model | editing = Just session }
            , Cmd.none
            )

        SessionEdition subMsg ->
            case model.editing of
                Just sessionBeingEdited ->
                    case subMsg of
                        SaveChanges ->
                            ( saveSession sessionBeingEdited model, Cmd.none )

                        _ ->
                            ( updateSessionEdition subMsg model, Cmd.none )

                Nothing ->
                    ( { model | editing = Nothing }, Cmd.none )


updateSessionEdition : SessionEditionMsg -> Model -> Model
updateSessionEdition msg model =
    { model
        | editing =
            case msg of
                UpdateDescription newDescription ->
                    Maybe.map
                        (\edition -> { edition | description = newDescription })
                        model.editing

                EarlierStartTime ->
                    Maybe.map
                        (\edition -> { edition | start = earlierBy model.options.granularity edition.start })
                        model.editing

                LaterStartTime ->
                    Maybe.map
                        (\edition -> { edition | start = laterBy model.options.granularity edition.start })
                        model.editing

                EarlierEndTime ->
                    Maybe.map
                        (\edition -> { edition | end = earlierBy model.options.granularity edition.end })
                        model.editing

                LaterEndTime ->
                    Maybe.map
                        (\edition -> { edition | end = laterBy model.options.granularity edition.end })
                        model.editing

                DiscardChanges ->
                    Nothing

                SaveChanges ->
                    -- we should never run into this case because
                    -- SaveChanges is handled before it reaches this function
                    -- (Not ideal!)
                    Nothing
    }


saveSession : Session -> Model -> Model
saveSession changedSession model =
    { model
        | sessions =
            model.sessions
                |> List.map
                    (\session ->
                        if session.id == changedSession.id then
                            changedSession

                        else
                            session
                    )
        , editing = Nothing
    }


view : Model -> Document Msg
view model =
    { title = "Oldtimer"
    , body =
        [ El.layout
            [ El.height El.fill
            , Background.color (El.rgb 1 1 1)
            , Font.size 16
            ]
            (body model)
        ]
    }


body : Model -> Element Msg
body model =
    let
        isTimerRunning =
            model.timerRunningSince /= Nothing

        pastSessions =
            List.map
                (viewSession model.timeZone model.now)
                model.sessions

        currentSession timeStart description =
            sessionRow
                { day = El.text "(en cours)"
                , duration = viewDuration timeStart model.now True
                , times =
                    El.text <|
                        viewTime model.timeZone timeStart
                            ++ " → …"
                , description = description
                , actions =
                    [ Input.button [ Region.description "Reculer l’heure de début", El.paddingXY 1 0 ]
                        { onPress = Just ExtendCurrentSession
                        , label = El.el [ El.centerY, El.centerX ] <| El.text earlierTimeSymbol
                        }
                    , Input.button [ Region.description "Avancer l’heure de début", El.paddingXY 1 0 ]
                        { onPress = Just ShortenCurrentSession
                        , label = El.text laterTimeSymbol
                        }
                    ]
                }
    in
    El.column
        [ El.width El.fill
        , El.height El.fill
        ]
        [ El.column
            [ El.width El.fill
            , El.padding 15
            , El.spacing 15
            , Border.shadow { offset = ( 0, 0 ), size = 0, blur = 5, color = rgb 0.7 0.7 0.7 }
            ]
            [ header model.timeZone model.now
            , El.el [ El.centerX ]
                (El.html (startStopButton isTimerRunning))
            , El.el [ El.centerX ]
                (status model)
            , El.el [ El.centerX ]
                (descriptionInput model.description UpdateCurrentDescription)
            ]
        , case model.editing of
            Just sessionBeingEdited ->
                El.el
                    [ El.centerX
                    , El.centerY
                    , El.width (El.fill |> El.maximum 500)
                    ]
                    (sessionEditionDialog model.timeZone sessionBeingEdited)

            Nothing ->
                El.column
                    [ El.width El.fill
                    , El.height El.fill
                    , El.centerX
                    , El.scrollbarY
                    ]
                    (case model.timerRunningSince of
                        Just timeStart ->
                            currentSession timeStart model.description :: pastSessions

                        Nothing ->
                            pastSessions
                    )
        ]


header : Time.Zone -> Time.Posix -> Element Msg
header timeZone now =
    El.row
        [ El.width El.fill
        , El.spaceEvenly
        , Font.size 13
        ]
        [ El.text <| viewTimeWithSeconds timeZone now
        , El.text <| viewDay timeZone now
        ]


startStopButton : Bool -> Html Msg
startStopButton isTimerRunning =
    let
        label =
            if isTimerRunning then
                "Stop"

            else
                "Start"

        msg =
            if isTimerRunning then
                Stop

            else
                Start
    in
    button [ onClick msg, classList [ ( "start-stop", True ), ( "start-stop--running", isTimerRunning ) ] ]
        [ div [ class "start-stop__icon" ] []
        , div [ class "start-stop__label" ] [ text label ]
        ]


status : Model -> Element Msg
status model =
    let
        isTimerRunning =
            model.timerRunningSince /= Nothing
    in
    El.el [] <|
        El.text
            (case model.timerRunningSince of
                Just time ->
                    let
                        durationString =
                            viewDuration time model.now False
                    in
                    "Session en cours "
                        ++ (if durationString /= "" then
                                " (" ++ durationString ++ ")"

                            else
                                ""
                           )

                Nothing ->
                    "Pas de session en cours"
            )


descriptionInput : String -> (String -> Msg) -> Element Msg
descriptionInput description msg =
    Input.text
        [ Border.width 1
        , Border.color transparent
        , Font.center
        , Font.italic
        , El.mouseOver
            [ Border.color (El.rgb 0.75 0.75 0.75) ]
        , El.width El.fill
        ]
        { onChange = msg
        , text = description
        , label = Input.labelHidden "Description"
        , placeholder =
            Just
                (Input.placeholder [] <|
                    El.el [ El.centerX, Font.italic ] <|
                        El.text "(description)"
                )
        }


viewSession : Time.Zone -> Time.Posix -> Session -> Element Msg
viewSession zone now session =
    let
        daySpan =
            absoluteDay zone session.end - absoluteDay zone session.start
    in
    sessionRow
        { day =
            viewDayFriendly zone session.start now
                |> withOptionalPlusOneDay (daySpan > 0)
        , duration = sessionDuration session
        , times =
            viewTime zone session.start
                ++ " → "
                ++ viewTime zone session.end
                |> withOptionalPlusOneDay (daySpan > 0)
        , description = session.description
        , actions = [ editSessionButton session, deleteSessionButton session.id ]
        }


withOptionalPlusOneDay : Bool -> String -> Element Msg
withOptionalPlusOneDay withPlus string =
    let
        optionalPlusOneDay =
            if withPlus then
                [ sup [] [ text "\u{00A0}+" ] ]

            else
                []
    in
    El.html <|
        span
            []
            (text string
                :: optionalPlusOneDay
            )


sessionRow : { day : Element Msg, duration : String, times : Element Msg, description : String, actions : List (Element Msg) } -> Element Msg
sessionRow { day, duration, times, description, actions } =
    El.el
        [ El.width El.fill
        , El.padding 15
        , Border.solid
        , Border.widthEach { bottom = 0, left = 0, right = 0, top = 1 }
        , Border.color (grayscale 0.8)
        ]
    <|
        El.row
            [ El.centerX
            , El.width (El.fill |> El.maximum 900)
            , El.spaceEvenly
            ]
            [ El.el [ El.width El.fill, Font.bold ] day
            , El.el [ El.width El.fill ] <| El.text duration
            , El.el [ El.width El.fill ] times
            , El.el [ El.width El.fill ] <| El.text description
            , El.row [ El.spacing 10 ] actions
            ]


grayscale : Float -> Color
grayscale value =
    rgb value value value


grayscale255 : Int -> Color
grayscale255 value =
    rgb255 value value value


sessionDuration : Session -> String
sessionDuration session =
    viewDuration session.start session.end False


sessionEditionDialog : Time.Zone -> Session -> Element Msg
sessionEditionDialog zone session =
    El.column
        [ El.spacing 15, El.width El.fill ]
        [ El.row [ El.width El.fill, El.spacing 15 ]
            [ El.column [ El.width (El.fillPortion 1), El.spacing 10 ]
                [ El.el [ El.centerX ] <| El.text "Début"
                , El.el [ El.centerX ] <| El.text <| viewTime zone session.start
                , El.row [ El.centerX, El.spaceEvenly, El.width El.fill ]
                    [ Input.button [ El.width El.fill, Region.description "Reculer l’heure de début" ]
                        { onPress = Just (SessionEdition EarlierStartTime)
                        , label = El.el [ El.centerY, El.centerX ] <| El.text earlierTimeSymbol
                        }
                    , Input.button [ El.width El.fill, Region.description "Avancer l’heure de début" ]
                        { onPress = Just (SessionEdition LaterStartTime)
                        , label = El.el [ El.centerY, El.centerX ] <| El.text laterTimeSymbol
                        }
                    ]
                ]
            , El.column [ El.width (El.fillPortion 1), El.spacing 5 ]
                [ El.el [ El.centerX ] <| El.text "⮕"
                , El.el [ El.centerX ] <|
                    El.text ("(" ++ viewDuration session.start session.end False ++ ")")
                ]
            , El.column [ El.width (El.fillPortion 1), El.spacing 10 ]
                [ El.el [ El.centerX ] <| El.text "Fin"
                , El.el [ El.centerX ] <| El.text <| viewTime zone session.end
                , El.row [ El.centerX, El.spaceEvenly, El.width El.fill ]
                    [ Input.button [ El.width El.fill, Region.description "Reculer l’heure de fin" ]
                        { onPress = Just (SessionEdition EarlierEndTime)
                        , label = El.el [ El.centerY, El.centerX ] <| El.text earlierTimeSymbol
                        }
                    , Input.button [ El.width El.fill, Region.description "Avancer l’heure de fin" ]
                        { onPress = Just (SessionEdition LaterEndTime)
                        , label = El.el [ El.centerY, El.centerX ] <| El.text laterTimeSymbol
                        }
                    ]
                ]
            ]
        , El.el [ El.centerX, El.width (El.fill |> El.maximum 250) ] <|
            descriptionInput session.description (SessionEdition << UpdateDescription)
        , El.row [ El.width El.fill ]
            [ Input.button [ El.width El.fill ]
                { onPress = Just (SessionEdition SaveChanges)
                , label = El.el [ El.centerX, El.centerY, El.padding 10 ] <| El.text "Enregistrer"
                }
            , Input.button [ El.width El.fill ]
                { onPress = Just (SessionEdition DiscardChanges)
                , label = El.el [ El.centerX, El.centerY, El.padding 10 ] <| El.text "Annuler"
                }
            ]
        ]


editSessionButton : Session -> Element Msg
editSessionButton session =
    Input.button [ Region.description "Modifier la session" ]
        { onPress = Just (EditSession session)
        , label = El.text "🖊"
        }


deleteSessionButton : Int -> Element Msg
deleteSessionButton id =
    Input.button [ Region.description "Supprimer la session" ]
        { onPress = Just (DeleteSession id)
        , label = El.text "❌"
        }


earlierTimeSymbol : String
earlierTimeSymbol =
    "⇠"


laterTimeSymbol : String
laterTimeSymbol =
    "⇢"


transparent : Color
transparent =
    El.rgba 0 0 0 0


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every 1000 SetCurrentTime


storableModel : Model -> StorableModel
storableModel model =
    { timerRunningSince = model.timerRunningSince |> Maybe.map Time.posixToMillis
    , description = model.description
    , sessions = model.sessions |> List.map toStorableSession
    , options = model.options
    , uid = model.uid
    }


toStorableSession : Session -> StorableSession
toStorableSession session =
    { start = Time.posixToMillis session.start
    , end = Time.posixToMillis session.end
    , description = session.description
    , id = session.id
    }


sessionFromStorable : StorableSession -> Session
sessionFromStorable storableSession =
    { start = Time.millisToPosix storableSession.start
    , end = Time.millisToPosix storableSession.end
    , description = storableSession.description
    , id = storableSession.id
    }


modelFromStorage : StorableModel -> Model -> Model
modelFromStorage savedState model =
    { model
        | timerRunningSince =
            savedState.timerRunningSince
                |> Maybe.map Time.millisToPosix
        , description = savedState.description
        , sessions =
            savedState.sessions
                |> List.map sessionFromStorable
        , options = savedState.options
        , uid = savedState.uid
    }


savedDataDecoder : Dec.Decoder StorableModel
savedDataDecoder =
    let
        timerRunningSinceDecoder =
            Dec.field "timerRunningSince" (Dec.nullable Dec.int)

        sessionDecoder : Dec.Decoder StorableSession
        sessionDecoder =
            Dec.map4 StorableSession
                (Dec.field "start" Dec.int)
                (Dec.field "end" Dec.int)
                (Dec.field "description" Dec.string)
                (Dec.field "id" Dec.int)

        sessionsDecoder : Dec.Decoder (List StorableSession)
        sessionsDecoder =
            Dec.field "sessions" (Dec.list sessionDecoder)

        optionsDecoder : Dec.Decoder Options
        optionsDecoder =
            Dec.field "options" (Dec.field "granularity" Dec.int)
                |> Dec.map Options
    in
    Dec.map5
        (\maybeInt description sessions options uid ->
            { timerRunningSince = maybeInt
            , description = description
            , sessions = sessions
            , options = options
            , uid = uid
            }
        )
        timerRunningSinceDecoder
        (Dec.field "description" Dec.string)
        sessionsDecoder
        optionsDecoder
        (Dec.field "uid" Dec.int)


decoderWithDefault : a -> Dec.Decoder a -> Dec.Decoder a
decoderWithDefault defaultValue decoder =
    Dec.oneOf [ decoder, Dec.succeed defaultValue ]


port saveToLocalStorage : StorableModel -> Cmd msg


port setFaviconTimerRunning : Bool -> Cmd msg


main : Program Enc.Value Model Msg
main =
    Browser.document
        { init = init
        , update = updateAndSave
        , view = view
        , subscriptions = subscriptions
        }
