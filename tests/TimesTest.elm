module TimesTest exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)
import Time
import Times exposing (absoluteDay, roundTime, viewDuration)


testAbsoluteDay : Test
testAbsoluteDay =
    describe "absoluteDay"
        [ test "..." <|
            \_ ->
                Time.millisToPosix 1611534086628
                    |> absoluteDay Time.utc
                    |> Expect.equal 18652
        , test "same day" <|
            \_ ->
                absoluteDay Time.utc (Time.millisToPosix 1611534086628)
                    == absoluteDay Time.utc (Time.millisToPosix 1611606086628)
                    |> Expect.true "Expected absoluteDay to be equal in both cases"
        , test "different day" <|
            \_ ->
                absoluteDay Time.utc (Time.millisToPosix 1611692486628)
                    - absoluteDay Time.utc (Time.millisToPosix 1611534086628)
                    |> Expect.equal 1
        ]


testViewDuration : Test
testViewDuration =
    let
        start =
            Time.millisToPosix 0
    in
    describe "viewDuration"
        [ test "format a duration with seconds correctly" <|
            \_ ->
                let
                    end =
                        Time.millisToPosix (42 * 1000)
                in
                Expect.equal
                    "42\u{00A0}s"
                    (viewDuration start end True)
        , test "format a duration with minutes and seconds correctly" <|
            \_ ->
                let
                    end =
                        Time.millisToPosix ((42 * 60 + 30) * 1000)
                in
                Expect.equal "42\u{00A0}min 30\u{00A0}s" (viewDuration start end True)
        , test "format a duration with hours, minutes and seconds correctly" <|
            \_ ->
                let
                    end =
                        Time.millisToPosix ((((2 * 60 + 42) * 60) + 30) * 1000)
                in
                Expect.equal "2\u{00A0}h 42\u{00A0}min 30\u{00A0}s" (viewDuration start end True)
        , test "format a duration without hours or seconds correctly" <|
            \_ ->
                let
                    end =
                        Time.millisToPosix ((33 * 60) * 1000)
                in
                Expect.equal "33\u{00A0}min" (viewDuration start end True)
        , test "format a duration without minutes correctly" <|
            \_ ->
                let
                    end =
                        Time.millisToPosix (((2 * 60 * 60) + 30) * 1000)
                in
                Expect.equal "2\u{00A0}h 30\u{00A0}s" (viewDuration start end True)
        , test "format a duration without seconds correctly" <|
            \_ ->
                let
                    end =
                        Time.millisToPosix (((2 * 60 + 42) * 60) * 1000)
                in
                Expect.equal "2\u{00A0}h 42\u{00A0}min" (viewDuration start end True)
        , test "format a duration without minutes or seconds correctly" <|
            \_ ->
                let
                    end =
                        Time.millisToPosix (2 * 60 * 60 * 1000)
                in
                Expect.equal "2\u{00A0}h" (viewDuration start end True)
        , test "returns a negative duration when end time is earlier than start time" <|
            \_ ->
                let
                    end =
                        Time.millisToPosix ((((2 * 60 + 42) * 60) + 30) * 1000)
                in
                -- end and start arguments have been swapped
                viewDuration end start True
                    |> Expect.equal "−\u{00A0}2\u{00A0}h 42\u{00A0}min 30\u{00A0}s"
        ]


testRoundTime : Test
testRoundTime =
    let
        minutes =
            (*) (60 * 1000) >> Time.millisToPosix

        fiveMinutes =
            5 * 60

        tenMinutes =
            10 * 60
    in
    describe "roundTime"
        [ test "closer to upper value" <|
            \_ ->
                roundTime fiveMinutes (18 |> minutes)
                    |> Expect.equal (20 |> minutes)
        , test "closer to lower value" <|
            \_ ->
                roundTime fiveMinutes (17 |> minutes)
                    |> Expect.equal (15 |> minutes)
        , test "already rounded" <|
            \_ ->
                roundTime fiveMinutes (25 |> minutes)
                    |> Expect.equal (25 |> minutes)
        , test "halfway betwen lower and upper values" <|
            \_ ->
                roundTime tenMinutes (45 |> minutes)
                    |> Expect.equal (40 |> minutes)
        ]
